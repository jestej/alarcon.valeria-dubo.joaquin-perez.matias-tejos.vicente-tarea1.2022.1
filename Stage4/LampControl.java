public class LampControl extends DomoticDeviceControl {
    public LampControl(int channel, Cloud c){
        super(channel, c);
        channel_update = channel;
        this.cloud =c;
    }
    public void pressPower(){
        cloud.changeLampPowerState(channel_update);
    }
    public void pressColor(String order){ // Cambiar el color de todas las lamparas en este canal.
        cloud.changeLampColor(channel_update,order);
    }
    public int getChannel(){
        return channel_update;
    }
    protected int channel_update;
}
