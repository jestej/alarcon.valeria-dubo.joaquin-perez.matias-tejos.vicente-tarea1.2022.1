import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<DomoticDevice>();
        rollerShades = new ArrayList<DomoticDevice>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public void addRollerShade(RollerShade rs){
        rollerShades.add(rs);
        
    }
    public void advanceTime(double delta){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            rs.advanceTime(delta);  
        }
    }
    private ArrayList<DomoticDevice> getDomoticDeviceAtChannel( ArrayList<DomoticDevice> devices, int channel){
        ArrayList<DomoticDevice> devicesTemp = new ArrayList<DomoticDevice>();
        for (DomoticDevice device : devices) {
            if(device.getChannel() == channel)
                devicesTemp.add(device);
        }

        return devicesTemp;
    }
    public void changeLampPowerState(int channel){

        ArrayList<DomoticDevice> lamparas = getDomoticDeviceAtChannel(lamps, channel);
        if (lamparas != null)
            for(DomoticDevice lampa : lamparas){
                ((Lamp) lampa).changePowerState();
            }
    }
    //________________________________________________________________
    public void changeLampColor(int channel, String order){
        ArrayList<DomoticDevice> lamparas = getDomoticDeviceAtChannel(lamps, channel);
        if (lamparas != null)
            for(DomoticDevice lampa : lamparas){
                ((Lamp) lampa).changeColor(order);
            }

    } 
    //________________________________________________________________
    public void startShadeUp(int channel){
        ArrayList<DomoticDevice> rollers = getDomoticDeviceAtChannel(rollerShades, channel);
        if (rollers != null)
            for(DomoticDevice roller : rollers){
                ((RollerShade) roller).startUp();
            }
        
    }
    
    //_________________________________________________________________
    public void startShadeDown(int channel){
        ArrayList<DomoticDevice> rollers = getDomoticDeviceAtChannel(rollerShades, channel);
        if (rollers != null)
            for(DomoticDevice roller : rollers){
                ((RollerShade) roller).startDown();
            }
       
    }
    public void stopShade(int channel){
        ArrayList<DomoticDevice> rollers = getDomoticDeviceAtChannel(rollerShades, channel);
        if (rollers != null)
            for(DomoticDevice roller : rollers){
                ((RollerShade) roller).stop();
            }
        
    }
    
    
    public String getHeaders(){
        String header = "";
        for (DomoticDevice  rs: rollerShades)
            header += ((RollerShade) rs).getHeader()+"\t";
        for (DomoticDevice l: lamps)
            header += l.getHeader()+"\t";
        return header;
    }
    public String getState(){
        String state = " ";
        
        for (DomoticDevice rs : rollerShades){
            state += rs.toString()+ "\t";
        }
        
        for (DomoticDevice l: lamps)
            state += l.toString()+"\t";
        return state;
    }
    private ArrayList<DomoticDevice> lamps;
    private ArrayList<DomoticDevice> rollerShades;
    protected int channel_llegada,channel_obtenido;
}
