public class LampControl extends DomoticDeviceControl {
    public LampControl(int channel, Cloud c){
        super(channel, c);
        this.cloud =c;
        this.channel = channel;
    }
    public void pressPower(int ch){
        cloud.changeLampPowerState(ch);
    }
    public void pressColor(int ch,String order){ // Cambiar el color de todas las lamparas en este canal.
        this.channel = ch;
        cloud.changeLampColor(ch,order);
    }
    public int getChannel(int ch){
        return ch;
    }
}
