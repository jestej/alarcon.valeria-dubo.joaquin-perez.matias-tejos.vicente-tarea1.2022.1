public class ShadeControl extends DomoticDeviceControl{
    public ShadeControl(int channel, Cloud c){
        super(channel,c);
        this.cloud =c;
        
    }
    public void startUp(int channel){
        cloud.startShadeUp(channel);
    }
    public void startDown(int channel){
        cloud.startShadeDown(channel);
    }
    public void stop(int channel){
        cloud.stopShade(channel);
    }
    private Cloud cloud;
    protected int ch;
}
