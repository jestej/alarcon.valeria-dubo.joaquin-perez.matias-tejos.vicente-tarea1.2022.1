public class Lamp extends DomoticDevice {
    public Lamp(int channel){
        super(id, channel);
        id_actual = getId();
        channel_actual = getChannel();
    }
    public void setChannel(int channel_actual){
        this.channel = channel_actual;
    }
    {
        id=nextId++;
        r = 0;
        g = 0;
        b = 0;
        r_prev=255;
        g_prev=255;
        b_prev=255;
        state = LampState.OFF;
    }
    
    public void changePowerState(){ // Antes de apagarse, se guarda el RGB actual para prenderse en ese mismo estado.
        if (state == LampState.ON){
            state = LampState.OFF;
            r_prev = r;
            g_prev = g;
            b_prev = b;
            r = 0;
            g = 0;
            b = 0;
        }
        else {
            state = LampState.ON;
            r = r_prev;
            g = g_prev;
            b = b_prev;
        }
    }// Antes de apagarse, se guarda el RGB actual
    public void geTID(){ 
        this.id = id_actual;
    }
    public void changeColor (String orden){
        switch(orden) { // Revisa todos los casos posibles y se encarga de que no se saturen a mas de 255.
            case "R U":
                r += 10;
                if (r > 255) {r = 255;}
                break;
            case "R D":
                r -= 10;
                if (r < 0){r = 0;}
                break;
            case "G U":
                g += 10;
                if (g > 255){g = 255;}
                break;
            case "G D":
                g -= 10;
                if (g < 0){g = 0;}
                break;
            case "B U":
                b += 10;
                if (b > 255){b = 255;}
                break;
            case "B D":
                b -= 10;
                if (b < 0){b = 0;}
                break;
            default:
                System.exit(-1);
        }

    }public String getHeader(){
        return "LR"+id_actual+"\tLG"+id_actual+"\ttLB"+id_actual;
    } // Retorna un String con el ID de esta lámpara.
    public String toString(){
        if (state==LampState.ON)
            return r+"\t"+g+"\t"+b;
        else
            return "0\t0\t0";
    }
    private enum LampState {
        ON,
        OFF
    }
    private short r,g,b;
    private LampState state;
    private short r_prev,g_prev,b_prev;
    private static int id;
    private static int nextId=1;
    protected int id_actual;
    private int channel_actual;
}