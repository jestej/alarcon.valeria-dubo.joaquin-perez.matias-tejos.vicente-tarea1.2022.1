import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Objects;

public class Operator {
    public Operator(Cloud c){
        Shadecontrol = new ArrayList<DomoticDeviceControl>();
        this.cloud = c;
        
    }
    public void addShadeControl(ShadeControl sc){
        Shadecontrol.add(sc);
    }
    private ArrayList<DomoticDeviceControl> getDomoticDeviceCAtChannel( ArrayList<DomoticDeviceControl> devicesC, int channel){
        ArrayList<DomoticDeviceControl> devicesTemp = new ArrayList<DomoticDeviceControl>();
        for (DomoticDeviceControl deviceC : devicesC) {
            if(deviceC.getChannel() == channel)
                devicesTemp.add(deviceC);
        }

        return devicesTemp;
    }
    public void executeCommands(Scanner in, PrintStream out){
        out.println("Time "+"\t"+ cloud.getHeaders());
        out.println((Math.round(time * 10) / 10)+"\t"+cloud.getState());
        while(in.hasNextInt()){
            int commandTime=in.nextInt();
            while (time < commandTime) {
                out.println((Math.round(time * 10) / 10)+"\t"+cloud.getState());
                cloud.advanceTime(delta);
                time+=delta;
            }
            String device=in.next();
            if (device.equals("C")){
                int channel=in.nextInt();
                String command= in.next();                  
                switch (command.charAt(0)){
                    // Todos las acciones que se pasan por el archivo son revisadas y se acciona la solicitada 
                    //System.out.println("Channel:"+channel_llegada+"Channel:llegada: "+channel_obtenido);
                   
                    case 'D':
                        ArrayList<DomoticDeviceControl> SC = getDomoticDeviceCAtChannel(Shadecontrol, channel);
                        if (SC != null)
                            for(DomoticDeviceControl sc : SC){
                                if(sc.getChannel() == channel)
                                    ((ShadeControl) sc).startDown(channel);
                            }
                        break;
                    case 'U':
                        ArrayList<DomoticDeviceControl> SC1 = getDomoticDeviceCAtChannel(Shadecontrol, channel);
                        if (SC1 != null)
                            for(DomoticDeviceControl sc : SC1){
                            if(sc.getChannel() == channel)
                                ((ShadeControl) sc).startUp(channel);
                            }
                        break;
                    case 'S':
                        ArrayList<DomoticDeviceControl> SC2 = getDomoticDeviceCAtChannel(Shadecontrol, channel);    
                        if (SC2 != null)
                            for(DomoticDeviceControl sc : SC2){
                            if(sc.getChannel() == channel)
                                ((ShadeControl) sc).stop(channel);
                        }
                        break;
                    default: out.println("Unexpected command:" + command);
                        System.exit(-1);
                }
            }//out.println(commandTime+"\t"+cloud.getState());
            else{
                out.println("Unexpected device:" + device);
                System.exit(-1);   
            }out.println((Math.round(time * 10) / 10)+"\t"+cloud.getState());
        }     
    }
    private double time=0;
    private Cloud cloud;
    private ArrayList<DomoticDeviceControl> Shadecontrol;
    private final double delta=0.1;
}
