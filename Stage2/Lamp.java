public class Lamp extends DomoticDevice {
    public Lamp(int channel){
        super(id, channel);
        channel = getChannel();
        id = getId();
    }
    {
        id=nextId++;
    }
    public void changePowerState(){
        if (state ==LampState.ON){
            state=LampState.OFF;
            r=0;
            g=0;
            b=0;
        }else{
            state=LampState.ON;
            r=255;
            g=255;
            b=255;
        }
    }
    public String getHeader(){
        return ""+id+"\t";
    }
    public String toString(){
        if (state==LampState.ON)
            return ""+r+"\t"+g+"\t"+b;
        else
            return "0\t0\t0";
    }
    private enum LampState {
        ON,
        OFF
    }
    private short r,g,b;
    private LampState state;
    private static int id;
    private static int nextId=0;
    private int channel;
}
