public class LampControl extends DomoticDeviceControl {
    public LampControl(int channel, Cloud c){
        super(channel, c);
        this.channel = channel;
        this.cloud =c;
    }
    public void pressPower(){
        cloud.changeLampPowerState(channel);
    }
}
