public class ShadeControl extends DomoticDeviceControl{
    public ShadeControl(int channel, Cloud c){
        super(channel,c);
        ch = getChannel();
    }
    public void startUp(){
        cloud.startShadeUp(ch);
    }
    public void startDown(){
        cloud.startShadeDown(ch);
    }
    public void stop(){
        cloud.stopShade(ch);
    }
    private Cloud cloud;
    protected int ch;
}
