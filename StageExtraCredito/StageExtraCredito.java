import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class StageExtraCredito{
    public static void main(String [] args) throws IOException {
        
        if (args.length != 2) {
            System.out.println("Usage: java Stage1 <configurationFile.txt> <channel>");
            System.exit(-1);
        }
        System.out.println("\n \n \t \t Domotic Devices HomeWork \t \t \n\n");
        Scanner in = new Scanner(new File(args[0]));
        Integer in_channel = Integer.parseInt(args[1]); 
        in.useLocale(Locale.ENGLISH);
        Cloud cloud = new Cloud();Operator operator = new Operator(cloud); 
        // reading <#_de_cortinas> <#_de_lámparas> <#_controles_cortinas> <#_controles_lámparas>
        // NUmeros-----------------------------------------
        int numRollerShades = in.nextInt(); //number of roller shades
        int numlamps= in.nextInt(); // number of lamps
        int numShadeControls = in.nextInt(); //Control shades
        int numLampControls = in.nextInt(); // skip number of lamp's controls
        //_____________________________________________________________________________
        // read <alfa0> <length0> <canal0> … <alfaN_1> <lengthN_1> <canalN_1>    
        
        for (int j = 0; j < numRollerShades;j++){
            // Se añaden las cortinas dependiendo de la cantidad existente.
            double alpha = in.nextDouble();
            double maxLength = in.nextDouble();
            int channel = in.nextInt();
            RollerShade rollerShade = new RollerShade(channel, alpha, maxLength);
            cloud.addRollerShade(rollerShade);
        }
        for (int i=0;i< numlamps;i++){
            // Se revisan la cantidad de lamparas que existen para asignar espacio de memoria respectivo junto a agregarlas
            int channel = in.nextInt();
            Lamp lamp = new Lamp(channel);
            cloud.addLamp(lamp);
        }
        for (int i = 0; i < numShadeControls; i++){
            //Se revisan los controles de cortinas y se le asigna su respectivo canal junto al espacio de memoria
            int channel = in.nextInt();
            ShadeControl shadeControl = new ShadeControl(channel, cloud);
            operator.addShadeControl(shadeControl);
            
        }//Se recorren todos los controles de la lampara y se les asigna el canal correspondiente
        for (int i = 0; i < numLampControls; i++){
            int channel = in.nextInt();
            LampControl lampControl = new LampControl(channel,cloud);
            operator.addLampControl(lampControl);
        }
        
        cloud.setChannelLS(in_channel);
        operator.executeCommands(in, System.out);

    }
    
}
