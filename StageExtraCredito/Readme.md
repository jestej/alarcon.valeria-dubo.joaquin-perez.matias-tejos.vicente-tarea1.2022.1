# Tarea 1 Elo-329 2022-1


### Integrantes

| ROL         | Alumno          |
|-------------|-----------------|
| 201521021-1 | Valeria Alarcón |
| 201521007-1 | Joaquín Dubó    |
| 201930011-3 | Matías Pérez    |
| 201930017-2 | Vicente Tejos   |

## Domotic Device RollerShades and Lamps 



##### [Guia de ayuda](http://profesores.elo.utfsm.cl/~agv/elo329/1s21/Assignments/guideline_2021-1/)
##### [Procedimientos de evaluacion](http://profesores.elo.utfsm.cl/~agv/elo329/1s21/Assignments/guideline_2021-1/html/normas_procedimientos.html)




# Se Realiza la Stage Bonus -> Se le ha llamado StageExtraCredito

# Se trabajo en gitlab la tarea para un mayor flujo de trabajo [Acceso Gitlab](https://gitlab.com/jestej/alarcon.valeria-dubo.joaquin-perez.matias-tejos.vicente-tarea1.2022.1)


# Cualquier problema para ingresar a git informar a: vicente.tejos@usm.cl o  +569 91528155

# TaskList

## Stage 1 (Manejo de I/O para una lamparas)

- [x] Se reciben los parametros correctamente del archivo para realizar el Stage
- [x] Generar salida de datos
- [x] Realizacion del makefile de la Stage
- [x] Cheaquear los datos de salida
- [x] Hacer comentarios relacionados a las implementaciones

## Stage 2 (Motor controlado de Cortina sin Tela )
- [x] Conectar cortinas con los controles.
- [x] Realizacion del makefile de la Stage
- [x] Cheaquear los datos de salida
- [x] Generar datos de salida
- [x] Hacer comentarios relacionados a las implementaciones

## Stage 3 (Lamparas y cortinas operando en conjunto sin tela ) 

- [x] Se reciben los parametros correctamente del archivo para realizar el Stage
- [x] Realizacion del makefile de la Stage
- [x] Hacer comentarios relacionados a las implementaciones

## Stage 4 (Dispositivos Domoticos con funcionalidad total) 
- [x] Se reciben los parametros correctamente del archivo para realizar el Stage
- [x] Hacer comentarios relacionados a las implementaciones
## Stage ExtraCredito
- [x] Stage Relizada.

## Documentacion
- [x] Realizar archivo de documentacion en pdf o html

# Compilación Intrucciones

- MakeFile:
    - make          _Para Compilar_ 
    - make run      _Para Ejecutar_
    - make clean    _Limpiar de archivos .class_

- Por consola:
	- para compilar:	`javac Stage1.java `
	- para ejecutar:	`java Stage1 configuration.txt > Stage1_salida.csv`


### STAGE 2

- Por consola:
	- para compilar:	`javac Stage2.java `
	- para ejecutar:	`java Stage2 configuration.txt > Stage2_salida.csv`

### STAGE 3

- Por consola:
	- para compilar:	`javac Stage3.java `
	- para ejecutar:	`java Stage3 configuration.txt > Stage3_salida.csv`

### STAGE 4

- Por consola:
	- para compilar:	`javac Stage4.java `
	- para ejecutar:	`java Stage4 configuration.txt > Stage4_salida.csv`

### Stage ExtraCredito
- Por consola:
    -para compilar:     `javac StageExtraCredito.java`

    # Recuerda que en esta stage se pide por consola que se ingrese un canal para implementar el LightSensor

    -para ejecutar:     `java StageExtraCredito configuration.txt 1`


NOTA: En caso de hacer la ejecucion sin el _makefile_ se pueden eliminar los archivos _.class_ usando el comando por consola `rm -r *.class` en la carpeta donde se requieran eliminar los archivos.

# Clases y sus descripciones

|Clase | Descripción |
|--|--|
| Stage1 | : Clase principal para ejecución de stage 1. |
| Stage2 | : Clase principal para ejecución de stage 2. |
| Stage3 | : Clase principal para ejecución de stage 3. |
| Stage4 | : Clase principal para ejecución de stage 4. |
| StageExtraCredito| : Clase principal para ejecutar StageExtraCredito.|
| Cloud  | : Clase que se encarga de determinar a que lamparas o cortinas va dirigida la orden en un respectivo canal. |
| Lamp |: Clase que contiene todas las funcionalidades de las lamparas, como su color actual y su estado.  |
| LampControl | : Clase que controla las lamparas de un unico canal, este no se puede cambiar. |
| Operator | : Clase que se encarga de leer el texto de entrada y ejecutar las ordenes. Tambien revisa si hay ordenes o dispositivos invalidos.  |
| DomoticDeviceControl | : Clase que controla los objetos domoticos.|
| ShadeControl| :  Clase que envia comandos de parada, subida y bajada de la cortina en el canal especificado a la nube.|
| LampState| : enum que contiene los dos estados posibles de una lámpara, ON y OFF. |
| RollerShade | :  Clase que contine todas las funcionalidades del motor de las cortinas.|
| DomoticDevice| : Clase Abstrcat que contiene los metodos fundamentales de los aparatos domoticos.|
| LightSensor| : Clase que se encarga de revisar si las lamparas deñ canal tienen la suma de sus componentes un valor mayor que 512 para cerrar las cortinas|
