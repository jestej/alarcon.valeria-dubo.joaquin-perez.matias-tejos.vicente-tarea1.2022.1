import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Operator {
    public Operator(Cloud c){
        Shadecontrol = new ArrayList<DomoticDeviceControl>();
        this.cloud = c;
        Lampcontrol =  new ArrayList<DomoticDeviceControl>();
    }
    public void addShadeControl(ShadeControl sc){
        Shadecontrol.add(sc);
    }
    public void addLampControl(LampControl lampControl){
        Lampcontrol.add(lampControl);
    }
    public void executeCommands(Scanner in, PrintStream out){
        out.println("Time "+"\t"+ cloud.getHeaders());
        out.println((Math.round(time * 10) / 10)+"\t"+cloud.getState());
        while(in.hasNextInt()){
            int commandTime=in.nextInt();
            while (time < commandTime) {
                out.println((Math.round(time * 10) / 10)+"\t"+cloud.getState());
                cloud.advanceTime(delta);
                time+=delta;
            }
            String device=in.next();
            if (device.equals("C")){
                int channel=in.nextInt();
                String command= in.next();                  
                switch (command.charAt(0)) {
                    case 'D':
                        cloud.startShadeDown(channel);
                        break;
                    case 'U':
                        cloud.startShadeUp(channel);
                        break;
                    case 'S':
                        cloud.stopShade(channel);
                        break;
                    default: out.println("Unexpected command:" + command);
                        System.exit(-1);
                }
            }
            else{

            if (device.equals("L")){
                int canal = in.nextInt();
                String orden = in.next();
                if (orden.equals("R") || orden.equals("G") || orden.equals("B")) { // Condicional para manejar instrucciones de color.
                    orden = orden + " " + in.next();
                }
                int exit = 0; // Variable para revisar error.
                for (String check : ordenes){
                    if (Objects.equals(orden, check)) { // Si la orden es valida exit = 1
                        exit = 1;
                        break;
                        }
                    }
                if (exit == 0){ // Si exit sigue siendo 0 el programa da error.
                    out.println("Unexpected order:" + orden);
                    System.exit(-1);
                }
                
                cloud.getLS().CheckRGB();
                if (orden.equals("P")){ // Orden de apagar/prender lámpara.
                    cloud.changeLampPowerState(canal);
                }
                else { // Orden de cambiar color.
                    cloud.changeLampColor(canal, orden);
                    
                }
            }
            else{
                out.println("Unexpected device:" + device);
                System.exit(-1);   
            }
            }out.println((Math.round(time * 10) / 10)+"\t"+cloud.getState());
        }     
    }
    private double time=0;
    private String ordenes[] = {"R U", "R D", "G U", "G D", "B U", "B D", "P"}; // órdenes admitidas
    
    private Cloud cloud;
    private ArrayList<DomoticDeviceControl> Shadecontrol;
    private ArrayList<DomoticDeviceControl> Lampcontrol;
    private final double delta=0.1;
}
