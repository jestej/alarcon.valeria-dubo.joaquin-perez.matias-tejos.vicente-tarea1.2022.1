public class Lamp {

    public Lamp (int channel){
        this.channel = channel;
    }
    {
        id=nextId++; // Se crea un ID unico para cada lampara que se crea. 
    }
    public int getChannel(){ // Retorna el canal de esta lampara. 
        return channel;
    }
    public void setChannel(int channel){ // Se asigna un canal nuevo a esta lampara. Notar que este metodo actualmente no se esta utilizando. 
        this.channel = channel;
    } //
    public void changePowerState(){ // Cambia el estado de la lampara. Notar que como actualmente solo debe prenderse y apagarse no se guarda el estado anterior de la lampara antes de apagarse. 
        if (state == LampState.ON){
            state = LampState.OFF;
            r = 0;
            g = 0;
            b = 0;
        }
        else {
            state = LampState.ON;
            r = 255;
            g = 255;
            b = 255;
        }
    }
    public String getHeader(){
        return "LR"+id+"\tLG"+id+"\tLB"+id;
    } // Retorna un String con el ID de esta lámpara.
    public String toString(){
        if (state==LampState.ON)
            return r+"\t"+g+"\t"+b;
        else
            return "0\t0\t0";
    }
    private int channel;
    private short r,g,b;
    private LampState state;
    private final int id;  // to conform lamp's header
    private static int nextId=0;
    
}
