import java.io.PrintStream;
import java.util.Scanner;

public class Operator {
    public Operator(LampControl lc, Cloud c){
        this.lampControl = lc;
        this.cloud = c;
    }
    public void executeCommands(Scanner in, PrintStream out){ // Ejecutar los comandos obtenidos en el archivo de texto. 
        out.println("Time\t" + cloud.getHeaders());
        out.println(time+"\t"+ cloud.getState());;
        while(in.hasNextInt()){
            time=in.nextInt();
            String string=in.next();
            if (!string.equals("L")) { // Se revisa si es un dispositivo valido. Actualmente solo revisa si es una lampara. 
                out.println("Unexpected device:" + string);
                System.exit(-1);
            }
            else {
                int canal = in.nextInt();
                String orden = in.next();

                if (!orden.equals("P")){ // Se revisa si es una orden valida. Actualmente solo revisa si la orden es "apretar boton de poder."
                    out.println("Invalid Order:" + orden);
                    System.exit(-1);
                }
                else{
                    lampControl.pressPower(); //Se ejecuta la orden. 
                    out.println(time+"\t"+cloud.getState());
                    }
            }
        }
    }
    private double time=0;
    private LampControl lampControl;
    private Cloud cloud;
}
