public class LampControl {
    public LampControl(int channel, Cloud c){
        this.channel = channel;
        this.cloud = c;
    }
    public void pressPower(){ // Invoca el metodo de cloud para cambiar el PowerState. 
        cloud.changeLampPowerState(channel);
    }
    public int getChannel(){ // Retorna el canal de este control. Actualmente este no puede ser cambiado. 
        return this.channel;
    }

    private int channel;
    private Cloud cloud;
}
