import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<Lamp>(); // Array donde se guardaran las lamparas. Actualmente solo se puede tener una lampara por canal.
    }
    public void addLamp(Lamp l){ // Se agrega una lampara nueva al Array. Actualmente no se puede elegir a que canal se agrega
        lamps.add(l);
    }
    public Lamp getLampAtChannel( int channel){ // Retorna la lampara en ese canal. 
        return lamps.get(channel);
    }
    public void changeLampPowerState(int channel){ // Llama al metodo de Lamp para prenderse/apagarse.
        lamps.get(channel).changePowerState();
    }
    public String getHeaders(){ // Retorna los ID de todas las lamparas en cada canal. Notar que esto no necesariamente va a ser creciente ni ordenado. 
        String header = "";
        for (Lamp l: lamps)
            header += l.getHeader();
        return header;
    }
    public String getState(){ // Retorna estado de las variables R,G y B de cada lampara, en forma de String. 
        String state = "";
        for (Lamp l: lamps){
            state += l.toString();
        }
        return state;
    }
    private ArrayList<Lamp> lamps; // getting ready for next stages
}
